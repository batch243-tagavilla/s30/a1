/* Total number of fruit on sale */

db.fruits.aggregate([
  {
    $match: { onSale: true },
  },
  {
    $count: "fruitsOnSale",
  },
]);

/* Total number of fruits with stock more than 20 */

db.fruits.aggregate([
  {
    $match: {
      stock: { $gt: 20 },
    },
  },
  {
    $count: "enoughStock",
  },
]);

/* Average price of fruits onSale per supplier */

db.fruits.aggregate([
  {
    $match: { onSale: true },
  },
  {
    $group: {
      _id: "$supplier_id",
      avgPrice: { $avg: "$price" },
    },
  },
]);

/* Highest price of fruit per supplier */

db.fruits.aggregate([
  {
    $group: {
      _id: "$supplier_id",
      maxPrice: { $max: "$price" },
    },
  },
]);

/* Lowest price of fruit per supplier */

db.fruits.aggregate([
  {
    $group: {
      _id: "$supplier_id",
      minPrice: { $min: "$price" },
    },
  },
]);
